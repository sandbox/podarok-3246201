#!/usr/bin/env bash

# Remove docroot folder from openm folder to avoid collision with drupal core.
# Script expects to be in openm/scripts/ but will check for openm.info.yml
# before removing any undesired docroot/ in the openm profile folder.
echo Removing docroot folder from openm project folder to avoid issues.
if [ -f "openm.info.yml" ]; then rm -rf docroot/ > /dev/null 2>&1; fi
