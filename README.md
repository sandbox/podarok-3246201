<p align="center">
 <!-- Will replace with new Open M logo soon. <a href="https://openm.org">
    <img alt="Open M Logo" src="" width="144">
  </a> -->
</p>

<h3 align="center">
  Open M on Drupal 9
</h3>
<p align="center">
  https://openm.org
</p>
<p align="center">
  An open source platform for OPENMs, by OPENMs built on <a href="https://drupal.org">Drupal</a>, ReactJS, Vue.js and others.
</p>

<p align="center">
  <a href="https://packagist.org/packages/openm/openm"><img src="https://img.shields.io/packagist/v/openm/openm.svg?style=flat-square"></a>
  <a href="https://packagist.org/packages/openm/openm"><img src="https://img.shields.io/packagist/dm/openm/openm.svg?style=flat-square"></a>
</p>

***

The Open M platform is a content management system that uses Drupal 9 functionality and useful modules from OPENMs and digital partners. It’s easy and free to use — everyone is welcome to implement Open M and run Open M projects.

# Demo
You can always evaluate distribution by visiting demo website - http://sandboxes-d9.openm.org. To get admin credentials to the sandbox - please [visit page for details](https://community.openmmca.org/t/how-can-i-try-or-get-a-demo-of-open-m/318).

![Open M Sandbox](docs/assets/sandbox.gif "Open M Demo")

*\* Open M version 1.3.*

# Installation

### Prerequisites

- Installed [Composer](https://getcomposer.org/download/)

### Installation of Open M
We are using [composer](https://getcomposer.org/) for initiating new project tree.

Development version of Open M on Drupal 9
```sh
composer create-project openm/openm-project:dev-9.2.x-development OPENM --no-interaction
```

Stable version of Open M on Drupal 9
```sh
composer create-project openm/openm-project OPENM --no-interaction
```

In order to properly install Open M Distribution, we have separate composer project - [openm-project](https://github.com/openm/openm-project). Please read detailed installation instructions [here](https://github.com/openm/openm-project).

# Development
For development environment please read documentation [openm-project](https://github.com/openm/openm-project#development-environment).

Other development information you can find at [docs/Development](https://github.com/openm/openm/tree/9.x-2.x/docs/Development).


# Documentation
- [Wiki](https://github.com/openm/openm/wiki)
- [Documentation](https://github.com/openm/openm/tree/9.x-2.x/docs)
- [Installation instructions](https://github.com/openm/openm-project#installation)
- [Content structure](https://github.com/openm/openm/tree/9.x-2.x/docs/Content%20structure)
- [Technical documentation](https://github.com/openm/openm/tree/9.x-2.x/docs/Development)
- [Terms of Use](https://github.com/openm/openm/wiki/open-m-Terms-of-Use)
- [Participation Agreement](https://github.com/openm/openm/wiki/open-m-Participant-Agreement)
