/**
 * @file
 * Open M Carnation JS.
 */
(function ($) {
  "use strict";
  Drupal.openmCarnation = Drupal.openmCarnation || {};

  /**
   * Alert Modals
   */
  Drupal.behaviors.openmAlertModals = {
    attach: function (context, settings) {
      var alertModals = $('.alert-modal', context);

      if (alertModals.length) {
        alertModals.on('hidden.bs.modal', function (e) {
          $(this).remove();
        });
        alertModals.modal('show');
      }
    }
  };
})(jQuery);
