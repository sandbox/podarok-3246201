<?php

/**
 * @file
 * Defines the Open M Profile install screen by modifying the install form.
 */

use Drupal\openm\Form\ConfigureProfileForm;
use Drupal\openm\Form\ContentSelectForm;
use Drupal\openm\Form\SearchSelectForm;
use Drupal\openm\Form\SearchSolrForm;
use Drupal\openm\Form\TermsOfUseForm;
use Drupal\openm\Form\ThemeSelectForm;
use Drupal\openm\Form\ThirdPartyServicesForm;
use Drupal\openm\Form\UploadFontMessageForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_install_tasks().
 */
function openm_install_tasks(&$install_state) {
  return [
    'openm_terms_of_use' => [
      'display_name' => t('Terms and Conditions'),
      'display' => TRUE,
      'run' => INSTALL_TASK_RUN_IF_REACHED
    ],
    'openm_select_search' => [
      'display_name' => t('Select search service'),
      'display' => TRUE,
      'type' => 'form',
      'function' => SearchSelectForm::class,
    ],
    'openm_install_search' => [
      'display_name' => t('Install search'),
      'type' => 'batch',
    ],
    'openm_solr_search' => [
      'display_name' => t('Configure Solr Search'),
      'display' => TRUE,
      'type' => 'form',
      'function' => SearchSolrForm::class,
    ],
    'openm_google_search' => [
      'type' => 'batch',
    ],
    'openm_select_features' => [
      'display_name' => t('Select installation type'),
      'display' => TRUE,
      'type' => 'form',
      'function' => ConfigureProfileForm::class,
    ],
    'openm_install_features' => [
      'type' => 'batch',
    ],
    'openm_select_theme' => [
      'display_name' => t('Select theme'),
      'display' => TRUE,
      'type' => 'form',
      'function' => ThemeSelectForm::class,
    ],
    'openm_install_theme' => [
      'type' => 'batch',
    ],
    'openm_select_content' => [
      'display_name' => t('Import demo content'),
      'display' => TRUE,
      'type' => 'form',
      'function' => ContentSelectForm::class,
    ],
    'openm_import_content' => [
      'type' => 'batch',
    ],
    'openm_set_frontpage' => [
      'type' => 'batch',
    ],
    'openm_discover_broken_paragraphs' => [
      'type' => 'batch',
    ],
    'openm_fix_configured_paragraph_blocks' => [
      'type' => 'batch',
    ],
    'openm_third_party_services' => [
      'display_name' => t('3rd party services'),
      'display' => TRUE,
      'type' => 'form',
      'function' => ThirdPartyServicesForm::class,
    ],
    'openm_upload_font_message' => [
      'display_name' => t('Read font info'),
      'display' => TRUE,
      'type' => 'form',
      'function' => UploadFontMessageForm::class,
    ],
    'openm_gtranslate_place_blocks' => [
      'type' => 'batch',
    ],
    'openm_install_finish' => [
      'type' => 'batch',
    ],
    'openm_terms_and_condition_db_save' => [
      'display' => FALSE,
    ],
  ];
}

/**
 * Create Google Translate block content.
 * Block already added from Open M Google Translate module configs.
 */
function openm_gtranslate_place_blocks(array &$install_state) {
  $moduleHandler = \Drupal::service('module_handler');
  if (!$moduleHandler->moduleExists('openm_gtranslate')) {
    return ['operations' => []];
  }

  $themes_list = [
    'openm_rose' => '5a698466-f499-4dda-a084-4d61c1d0e902',
    'openm_lily' => '5a698466-f499-4dda-a084-4d61c1d0e777',
    'openm_carnation' => '32fa8958-20d7-41e0-9e7c-a5768bf6dfac',
  ];
  /** @var \Drupal\Core\Entity\EntityTypeManager $entityTypeManager */
  $entityTypeManager = \Drupal::service('entity_type.manager');
  foreach ($themes_list as $theme => $uuid) {
    /** @var \Drupal\block_content\Entity\BlockContent $blockContent */
    $blockContent = $entityTypeManager->getStorage('block_content')->create([
      'type' => 'openm_gtranslate_block',
      'info' => t('Google Translate Block'),
      'uuid' => $uuid,
    ]);
    $blockContent->save();
  }
  return ['operations' => []];
}

/**
 * Mapping for demo content configs.
 *
 * @param null|string $key
 *   Name of the section with demo content.
 *
 * @return array
 *   Mapping array.
 */
function openm_demo_content_configs_map($key = NULL) {
  // Maps installation presets to demo content.
  $map = [
    'core' => [
      'openm_demo_taxonomy',
      'openm_demo_tcolor',
    ],
    'complete' => [
      'openm_demo_nalert',
      'openm_demo_nbranch',
      'openm_demo_ncamp',
      'openm_demo_nblog',
      'openm_demo_nnews',
      'openm_demo_nevent',
      'openm_demo_nfacility',
      'openm_demo_nlanding',
      'openm_demo_nmbrshp',
      'openm_demo_nprogram',
      'openm_demo_ncategory',
      'openm_demo_nclass',
      'openm_demo_nsessions',
      'openm_demo_menu',
      'openm_demo_menu_main',
      'openm_demo_menu_footer',
      'openm_demo_webform',
      'openm_demo_ahb',
      'openm_demo_nsocial_post',
      'openm_demo_tarea',
      'openm_demo_tblog',
      'openm_demo_tnews',
      'openm_demo_tfacility',
      'openm_demo_tamenities',
      'openm_demo_bfooter',
      'openm_demo_bmicrosites_menu',
      'openm_demo_addthis',
      'openm_demo_bsimple',
      'openm_demo_bamenities',
      'openm_demo_tfitness',
    ],
    'standard' => [
      'openm_demo_nalert',
      'openm_demo_nlanding',
      'openm_demo_menu',
      'openm_demo_nnews',
      'openm_demo_menu_main',
      'openm_demo_menu_footer',
      'openm_demo_webform',
      'openm_demo_ahb',
      'openm_demo_tcolor',
      'openm_demo_tamenities',
      'openm_demo_bfooter',
      'openm_demo_taxonomy',
    ],
    'extended' => [
      'openm_demo_nalert',
      'openm_demo_nbranch',
      'openm_demo_nevent',
      'openm_demo_nnews',
      'openm_demo_nlanding',
      'openm_demo_nmbrshp',
      'openm_demo_menu',
      'openm_demo_menu_main',
      'openm_demo_menu_footer',
      'openm_demo_webform',
      'openm_demo_ahb',
      'openm_demo_tnews',
      'openm_demo_tcolor',
      'openm_demo_tarea',
      'openm_demo_tamenities',
      'openm_demo_bfooter',
      'openm_demo_addthis',
      'openm_demo_bsimple',
      'openm_demo_bamenities',
      'openm_demo_taxonomy',
    ],

  ];

  return array_key_exists($key, $map) ? $map[$key] : [];
}

/**
 * Create batch for enabling features.
 *
 * @param array $install_state
 *   Installation parameters.
 *
 * @return array
 *   Batch.
 */
function openm_install_features(array &$install_state) {
  $module_operations = [];

  $preset = $install_state['openm']['preset'];
  \Drupal::state()->set('openm_preset', $preset);
  $modules = ConfigureProfileForm::getModulesToInstallWithDependencies($preset);

  foreach ($modules as $module) {
    $module_operations[] = ['openm_enable_module', (array) $module];
  }

  return ['operations' => $module_operations];
}

/**
 * Create batch for installing search.
 *
 * @param array $install_state
 *   Installation parameters.
 *
 * @return array
 *   Batch.
 */
function openm_install_search(array &$install_state) {
  $state = \Drupal::state();
  $module = $install_state['openm']['search']['service'];
  $files = \Drupal::service('extension.list.module')->getList();
  if (isset($install_state['openm']['search']['search_api_server'])) {
    $server = $install_state['openm']['search']['search_api_server'];
    if ($module == 'openm_search_api' && $server == 'solr') {
      $state->set('openm_show_solr_config', '1');
    }
  };

  if (isset($install_state['openm']['search']['google_search_engine_id'])) {
    $state->set('google_search_engine_id', $install_state['openm']['search']['google_search_engine_id']);
  };
  if ($files[$module]->requires) {
    $modules = array_merge(array_keys($files[$module]->requires), (array) $module);
  }
  foreach ($modules as $module) {
    $module_operations[] = ['openm_enable_module', (array) $module];
  }
  // todo: install search_api_solr_legacy.
  $module_operations[] = ['openm_enable_search_api_solr_legacy', []];

  return ['operations' => $module_operations];
}

/**
 * Create batch for write google custom search id to configuration.
 *
 * @param array $install_state
 *   Installation parameters.
 */
function openm_google_search(array &$install_state) {
  // Set Google Custom Search Engine ID.
  if (!empty(\Drupal::state()->get('google_search_engine_id'))) {
    $config_factory = \Drupal::configFactory();
    $config = $config_factory->getEditable('openm_google_search.settings');
    $config->set('google_engine_id', \Drupal::state()->get('google_search_engine_id'));
    $config->save();
  }
}

/**
 * Create batch for install and set default theme.
 *
 * @param array $install_state
 *   Installation parameters.
 *
 * @return array
 *   Batch.
 */
function openm_install_theme(array &$install_state) {
  $theme = $install_state['openm']['theme'];
  if (function_exists(  'drush_print')) {
    drush_print(dt('Theme: %theme', ['%theme' => $theme]));
  }
  $config_factory = Drupal::configFactory();
  // Set the default theme.
  $config_factory
    ->getEditable('system.theme')
    ->set('default', $theme)
    ->save(TRUE);
  $theme_operations[] = ['openm_enable_theme', (array) $theme];
  return ['operations' => $theme_operations];
}

/**
 * Create batch for content import.
 *
 * @param array $install_state
 *   Installation parameters.
 *
 * @return array
 *   Batch.
 */
function openm_import_content(array &$install_state) {
  $module_operations = [];
  $migrate_operations = [];
  $uninstall_operations = [];
  $preset = \Drupal::state()->get('openm_preset') ?: 'complete';
  if (function_exists('drush_print')) {
    drush_print(dt('Preset: %preset', ['%preset' => $preset]));
  }
  $preset_tags = [
    'standard' => 'openm_standard_installation',
    'extended' => 'openm_extended_installation',
    'complete' => 'openm_complete_installation',
  ];
  $migration_tag = $preset_tags[$preset];
  // Add core demo content.
  _openm_import_content_helper($module_operations, 'core', 'enable');
  $migrate_operations[] = ['openm_import_migration', ['openm_core_installation']];

  if ($install_state['openm']['content']) {
    // If option has been selected build demo modules installation operations array.
    _openm_import_content_helper($module_operations, $preset, 'enable');
    // Add migration import by tag to migration operations array.
    $migrate_operations[] = ['openm_import_migration', (array) $migration_tag];
    if ($preset == 'complete') {
      // Add demo content Program Event Framework landing pages manually.
      // Do it as the last step so menu items are in place.
      $migrate_operations[] = ['openm_demo_nlanding_pef_pages', []];
      // Add demo content Activity Finder landing pages manually.
      // Do it as the last step so menu items are in place.
      $migrate_operations[] = ['openm_demo_nlanding_af_pages', []];
    }
    // Build demo modules uninstall array to disable migrations with demo content.
    _openm_import_content_helper($uninstall_operations, $preset, 'uninstall');
    if (function_exists('drush_print')) {
      drush_print(dt('Demo content enabled'));
    }
  }
  else {
    // Add homepage alternative if demo content is not enabled.
    $module_operations[] = ['openm_enable_module', (array) 'openm_demo_nhome_alt'];
    $migrate_operations[] = ['openm_import_migration', (array) 'openm_demo_home_alt'];
    $uninstall_operations[] = ['openm_uninstall_module', (array) 'openm_demo_home_alt'];
  }

  // Uninstall core demo content modules.
  _openm_import_content_helper($uninstall_operations, 'core', 'uninstall');
  // Combine operations module enable before of migrations.
  return ['operations' => array_merge($module_operations, $migrate_operations, $uninstall_operations)];
}

/**
 * Set the homepage whether from demo content or default one.
 */
function openm_set_frontpage(array &$install_state) {
  // Set homepage by node id but checking it first by title only.
  $query = \Drupal::entityQuery('node')
    ->condition('status', 1)
    ->condition('title', 'Open M');
  $nids = $query->execute();
  $config_factory = Drupal::configFactory();
  $config_factory->getEditable('system.site')->set('page.front', '/node/' . reset($nids))->save();

  return ['operations' => []];
}

/**
 * Fix broken paragraphs which for some reason weren't discovered.
 *
 * @see https://www.drupal.org/node/2889297
 * @see https://www.drupal.org/node/2889298
 */
function openm_discover_broken_paragraphs(array &$install_state) {
  /**
   * Reset data for broken paragraphs using block fields from plugin module.
   *
   * @param array $tables
   * @param string $plugin_id_field
   * @param string $config_field
   */
  $process_paragraphs = function (array $tables, $plugin_id_field, $config_field) {
    foreach ($tables as $table) {
      if (!\Drupal::database()->schema()->tableExists($table)) {
        continue;
      }
      // Select all paragraphs that have "broken" as plugin_id.
      $query = \Drupal::database()->select($table, 'ptable');
      $query->fields('ptable');
      $query->condition('ptable.' . $plugin_id_field, 'broken');
      $broken_paragraphs = $query->execute()->fetchAll();

      // Update to correct plugin_id based on data array.
      foreach ($broken_paragraphs as $paragraph) {
        $data = unserialize($paragraph->{$config_field});
        $query = \Drupal::database()->update($table);
        $query->fields([
          $plugin_id_field => $data['id'],
        ]);
        $query->condition('bundle', $paragraph->bundle);
        $query->condition('entity_id', $paragraph->entity_id);
        $query->condition('revision_id', $paragraph->revision_id);
        $query->condition('langcode', $paragraph->langcode);
        $query->execute();
      }
    }
  };

  $process_paragraphs([
    'paragraph__field_prgf_block',
    'paragraph_revision__field_prgf_block',
  ],
    'field_prgf_block_plugin_id',
    'field_prgf_block_plugin_configuration'
  );
  $process_paragraphs([
    'paragraph__field_prgf_schedules_ref',
    'paragraph_revision__field_prgf_schedules_ref',
  ],
    'field_prgf_schedules_ref_plugin_id',
    'field_prgf_schedules_ref_plugin_configuration'
  );
  $process_paragraphs([
    'paragraph__field_prgf_location_finder',
    'paragraph_revision__field_prgf_location_finder',
  ],
    'field_prgf_location_finder_plugin_id',
    'field_prgf_location_finder_plugin_configuration'
  );
  $process_paragraphs([
    'paragraph__field_prgf_location_finder',
    'paragraph_revision__field_prgf_location_finder',
  ],
    'field_prgf_location_finder_plugin_id',
    'field_prgf_location_finder_plugin_configuration'
  );
  $process_paragraphs([
    'paragraph__field_branch_contacts_info',
    'paragraph_revision__field_branch_contacts_info',
  ],
    'field_branch_contacts_info_plugin_id',
    'field_branch_contacts_info_plugin_configuration'
  );
}

/**
 * Add Block configuration to Branch demo content Group Schedules paragraphs.
 *
 * @see openm_discover_broken_paragraphs().
 */
function openm_fix_configured_paragraph_blocks(array &$install_state) {
  $tables = [
    'paragraph__field_prgf_schedules_ref',
    'paragraph_revision__field_prgf_schedules_ref',
  ];

  foreach ($tables as $table) {
    if (!\Drupal::database()->schema()->tableExists($table)) {
      continue;
    }
    $query = \Drupal::database()
      ->select($table, 'ptable');
    $query->fields('ptable');
    $query->condition('ptable.bundle', 'group_schedules');
    $query->join('node__field_content', 'content', 'content.field_content_target_id = ptable.entity_id');
    $query->condition('content.bundle', 'branch');
    $group_schedule_paragraphs = $query->execute()->fetchAll();

    $location_ids = ['1036', '204', '203', '202'];
    // Update to correct plugin_id based on data array.
    foreach ($group_schedule_paragraphs as $paragraph) {
      $data = unserialize($paragraph->field_prgf_schedules_ref_plugin_configuration);
      $data['enabled_locations'] = array_pop($location_ids);
      $data['label_display'] = 0;
      $query = \Drupal::database()->update($table);
      $query->fields([
        'field_prgf_schedules_ref_plugin_configuration' => serialize($data),
      ]);
      $query->condition('bundle', $paragraph->bundle);
      $query->condition('entity_id', $paragraph->entity_id);
      $query->condition('revision_id', $paragraph->revision_id);
      $query->condition('langcode', $paragraph->langcode);
      $query->execute();
    }
  }
}

/**
 * Run final Open M profile install procedures.
 */
function openm_install_finish(array &$install_state) {
  // Rerun install all available optional config that appeared after
  // installation of Open M modules and themes.
  // We need to run this because during a drupal installation
  // optional configuration is installed only once at the
  // end of the core installation process.
  // \Drupal::service('config.installer')->installOptionalConfig();
  // Disable the default 'frontpage' Views configuration since it is unused. Due to the fact that this Views
  // configuration is stored in the optional folder there is no way to disable it before optional configs are installed.
  $view = \Drupal::entityTypeManager()->getStorage('view')->load('frontpage');
  if ($view) {
    $view->disable();
    $view->save();
  }
}

/**
 * Demo content import helper.
 *
 * @param array $module_operations
 *   List of module operations.
 * @param string $key
 *   Key of the section in the mapping.
 * @param string $action
 *   Action for demo content modules: enable|uninstall.
 */
function _openm_import_content_helper(array &$module_operations, string $key, string $action) {
  $modules = openm_demo_content_configs_map($key);
  if (empty($modules)) {
    return;
  }
  foreach ($modules as $module) {
    $module_operations[] = ['openm_' . $action . '_module', (array) $module];
  }
}

/**
 * Enable module.
 *
 * @param string $module_name
 *   Module name.
 *
 * @throws \Drupal\Core\Extension\MissingDependencyException
 */
function openm_enable_module($module_name) {
  /** @var \Drupal\Core\Extension\ModuleInstaller $service */
  $service = \Drupal::service('module_installer');
  $service->install([$module_name]);
}

/**
 * Enable theme.
 *
 * @param string $theme_name
 *   Module name.
 *
 * @throws \Drupal\Core\Extension\ExtensionNameLengthException
 */
function openm_enable_theme($theme_name) {
  /** @var \Drupal\Core\Extension\ThemeInstaller $service */
  $service = \Drupal::service('theme_installer');
  $service->install([$theme_name]);
}

/**
 * Uninstall module.
 *
 * @param string $module_name
 *   Module name.
 */
function openm_uninstall_module($module_name) {
  /** @var \Drupal\Core\Extension\ModuleInstaller $service */
  $service = \Drupal::service('module_installer');
  $service->uninstall([$module_name]);
}

/**
 * Import migrations with specified tag.
 *
 * @param string $migration_tag
 *   Migration tag.
 */
function openm_import_migration($migration_tag) {
  $importer = \Drupal::service('openm_migrate.importer');
  $importer->importByTag($migration_tag);
}

/**
 * Implements hook_form_FORM_ID_alter.
 *
 * This will change the description text for the site slogan field.
 *
 * @param $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 * @param $form_id
 */
function openm_form_system_site_information_settings_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  $form['site_information']['site_slogan']['#description'] = t("This will display your association name in the header as per Y USA brand guidelines. Try to use less than 27 characters. The text may get cut off on smaller devices.");
}

/**
 * Implements hook_preprocess_block().
 */
function openm_preprocess_block(&$variables) {
  $variables['base_path'] = base_path();

  // Prevent some blocks from caching
  $preventCacheBlocks = [
    'system_breadcrumb_block',
  ];
  if (in_array($variables['plugin_id'], $preventCacheBlocks)) {
    $variables['#cache']['max-age'] = 0;
  }
}

/**
 * Implements hook_form_FORM_ID_alter.
 *
 * Add description how to use CSS Editor on the theme configuration page.
 */
function openm_form_system_theme_settings_alter(&$form, FormStateInterface $form_state, $form_id) {
  if (isset($form['css_editor'])) {
    // Add short manual how to use CSS Editor inside the theme.
    $types = \Drupal::entityTypeManager()->getStorage('node_type')->loadMultiple();
    $css_node_selectors = array_map(function ($type) {
      return str_replace('_', '-', $type);
    }, array_keys($types));

    $css_editor_info = [
      '#prefix' => '<div class="description">',
      '#markup' => t('In order to change CSS on each particular page you
      should use the following selectors:<br/>
      - .page-node-type-{node type};<br/>
      - .node-id-{node ID};<br/>
      - .path-frontpage.<br/><br/>
      The existing node types are: ' . implode(', ', $css_node_selectors) . '.
      '),
      '#suffix' => '</div>'
    ];
    $form['css_editor']['css_editor_info'] = $css_editor_info;
  }
}

/**
 * Implements hook_help().
 *
 * Add description how to use CSS Editor to help.
 */
function openm_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'system.theme_settings_theme':
      $theme = $route_match->getParameter('theme');
      $config = \Drupal::configFactory()->getEditable('css_editor.theme.' . $theme);

      if ($config->get('enabled')) {
        return '<p>' . t('If you need to change CSS on some pages independently, you may use Custom CSS configuration.') . '</p>';
      }
      else {
        return '<p>' . t('If you need to change CSS on some pages independently, you should enable Custom CSS functionality.') . '</p>';
      }

      break;
  }
}

/**
 * Implements hook_install_tasks_alter().
 */
function openm_install_tasks_alter(&$tasks, &$install_state) {
  $new_tasks = [];

  // Looks like we don't have another way to put T&C on the first page.
  $new_tasks['openm_terms_of_use'] = $tasks['openm_terms_of_use'];
  $tasks = array_merge($new_tasks, $tasks);

  // Remove 3rd party services installation task for standard preset.
  if (!empty(\Drupal::state()->get('openm_preset')) &&
    \Drupal::state()->get('openm_preset') == 'standard' &&
    isset($tasks["openm_third_party_services"])) {
      unset($tasks["openm_third_party_services"]);
  }
  // Remove Solr configure installation task for non search_api sorl service.
  if (!\Drupal::state()->get('openm_show_solr_config')) {
    unset($tasks["openm_solr_search"]);
  }

  if (isset($install_state['openm']['search']['service']) &&
    $install_state['openm']['search']['service'] == 'none' ) {
    unset($tasks["openm_install_search"]);
    unset($tasks["openm_solr_search"]);
  }

}

/**
 * Displays the Terms and Conditions form.
 *
 * @param $install_state
 *   An array of information about the current installation state. The T&C flag
 *   will be added here.
 *
 * @return mixed
 *   A T&C form if T&C has not been accepted.
 */
function openm_terms_of_use(&$install_state) {
  // Open M classes are not included yet on the first installation page,
  // because profile is not installed.
  // That's why we should include T&C form manually.
  if (!class_exists('\Drupal\openm\Form\TermsOfUseForm')) {
    $path = drupal_get_path('profile', 'openm');
    require_once $path . '/src/Form/TermsOfUseForm.php';
  }

  if (!empty($install_state['parameters']['terms_and_conditions'])) {
    return;
  }

  if ($install_state['interactive']) {
    return install_get_form('Drupal\openm\Form\TermsOfUseForm', $install_state);
  }
}

/**
 * Saves T&C accepted version to db. We can't do it on the first step,
 * because db is not configured yet.
 *
 * @param $install_state
 *   An array of information about the current installation state.
 */
function openm_terms_and_condition_db_save(&$install_state) {
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable('openm.terms_and_conditions.schema');
  $config->set('version', TermsOfUseForm::TERMS_OF_USE_VERSION);
  $config->set('accepted_version', time());
  $config->save();
}

/**
 * Enables Search API Solr Legacy if Solr 4.x backend is detected.
 *
 * @return bool
 *   TRUE if the legacy module was enabled.
 */
function openm_enable_search_api_solr_legacy() {
  $moduleHandler = \Drupal::service('module_handler');
  if (!$moduleHandler->moduleExists('search_api_solr')) {
    return;
  }
  $storage = \Drupal::entityTypeManager()
    ->getStorage('search_api_server');
  $search_api_servers = $storage->getQuery()->execute();

  foreach ($search_api_servers as $search_api_server_id) {
    $search_api_server = $storage->load($search_api_server_id);
    if ($search_api_server->getBackendId() !== 'search_api_solr') {
      continue;
    }
    if (!$search_api_server->hasValidBackend()) {
      continue;
    }
    $connector = $search_api_server->getBackend()->getSolrConnector();
    $solr_version = $connector->getSolrVersion();
    $solr_version_forced = $connector->getSolrVersion(TRUE);
    if (preg_match('/[45]{1}\.[0-9]+\.[0-9]+/', $solr_version)
      || preg_match('/[45]{1}\.[0-9]+\.[0-9]+/', $solr_version_forced)) {
      // Solr 4.x or 5.x found, install the Search API Solr Legacy module.
      \Drupal::service('module_installer')->install(['search_api_solr_legacy']);
      return TRUE;
    }
  }

  return FALSE;
}
