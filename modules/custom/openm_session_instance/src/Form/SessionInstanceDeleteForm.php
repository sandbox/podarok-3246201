<?php

namespace Drupal\openm_session_instance\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Session Instance entities.
 *
 * @ingroup openm_session_instance
 */
class SessionInstanceDeleteForm extends ContentEntityDeleteForm {


}
