### Welcome to Open M Continuous Integration module

This module aims to help with Continuous Integration for the Open M distribution.
Initially module used for enabling domains with different theme(design) per domain.

Example:
 - openm.cibox.tools/buildXXX shows Rose theme as default for the site and Seven for admin interface
 - lily.openm.cibox.tools/buildXXX shows Lily theme as default for the site and Seven for admin interface
 
