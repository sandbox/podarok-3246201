<?php

/**
 * @file
 * OpenM System module file.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Perform necessary actions after OpenM modules are uninstalled.
 *
 * This function differs from hook_uninstall() in that it gives all other
 * modules a chance to perform actions when a module is uninstalled, whereas
 * hook_uninstall() is only called on the module actually being uninstalled.
 *
 * @param array $modules
 *   An array of the modules that were uninstalled.
 *
 * @see hook_uninstall()
 */
function openm_system_modules_uninstalled(array $modules) {
  $modules_manager = \Drupal::service('openm.modules_manager');
  foreach ($modules as $module) {
    if (strpos($module, 'openm_') !== FALSE) {
      // Run post uninstall only for openm modules.
      $modules_manager->postUninstall($module);
      // Additionally destroy migration data for openm modules.
      $modules_manager->destroyMigrationData($module);
    }
  }
}

/**
 * Implements hook_page_attachments().
 */
function openm_system_page_attachments(array &$attachments) {
  // Open M System admin menu CSS.
  $attachments['#attached']['library'][] = 'openm_system/openm_system.admin_menu';
}

/**
 * Implements hook_help().
 */
function openm_system_help($route_name, RouteMatchInterface $route_match) {
  if ($route_name == 'openm_system.modules_uninstall') {
    return '<p>' . t('The uninstall process removes all data related to a package.') . '</p>';
  }
}

/**
 * Alters Drupal themes.
 *
 * @param $theme_groups
 *   An associative array containing groups of themes.
 *
 * @see system_themes_page()
 */
function openm_system_system_themes_page_alter(&$theme_groups) {
  foreach ($theme_groups as $state => &$group) {
    foreach ($theme_groups[$state] as $key => &$theme) {
      if (!Drupal::currentUser()->hasPermission('use all drupal themes')) {
        if ($theme->origin === 'core' || $theme->info['name'] === 'Open M Admin') {
          unset($theme_groups[$state][$key]);
        }
      }
    }
  }
}
