<?php

/**
 * @file
 * Hooks specific to the openm_search module.
 */

/**
 * Alter the default Open M themes search form parameters.
 *
 * Search parameters for theme originally stored in theme_name.settings file.
 *
 * @param array $search_config
 *   The associative array of search form parameters.
 *
 * @see \Drupal\openm_search\Config\OpenmSearchOverrides::loadOverrides()
 */
function hook_openm_search_theme_configuration_alter(&$search_config) {
  $search_config = [
    'search_query_key' => 'q',
    'search_page_alias' => 'search',
    'display_search_form' => 1,
  ];
}
