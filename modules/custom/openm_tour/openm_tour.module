<?php

/**
 * @file
 * Contains openm_tour.module.
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\Markup;
use Drupal\Core\Asset\AttachedAssetsInterface;

/**
 * Implements hook_token_info().
 */
function openm_tour_token_info() {
  $info['tokens']['openm_tour']['click'] = [
    'name' => t('Open M Tour Click'),
    'description' => t('Provides a token which replaced by button with target selector to click.'),
  ];
  return $info;
}

/**
 * Implements hook_tokens().
 */
function openm_tour_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  if ($type == 'openm_tour') {
    foreach ($tokens as $name => $original) {
      list($token_name, $selector) = explode(':', $name, 2);
      if (!empty($selector)) {
        switch ($token_name) {
          case 'click':
            $markup = Markup::create('<p><a href="#" class="button--primary button openm-click-button" data-tour-selector="' . $selector . '">' . t('Next') . '</a></p>');
            $replacements[$original] = $markup;
            break;
        }
      }
    }
  }
  return $replacements;
}

/**
 * Implements hook_page_attachments_alter().
 */
function openm_tour_page_attachments_alter(array &$attachments) {
  $attachments['#attached']['library'][] = 'openm_tour/openm-tour';
}

/**
 * Implements hook_js_alter().
 */
function openm_tour_js_alter(&$javascript, AttachedAssetsInterface $assets) {
  if (isset($javascript['core/modules/tour/js/tour.js'])) {
    $javascript['core/modules/tour/js/tour.js']['data'] = drupal_get_path('module', 'openm_tour') . '/js/tour.js';
  }
}
