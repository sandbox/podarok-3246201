<?php

/**
 * @file
 * Hooks and routines for the openm_update module.
 */

/**
 * Endpoint to check OpenM packages.
 */
define('openm_UPDATE_PACKAGES_ENDPOINT', 'http://openm.org:1880/packages');

/**
 * Get list of projects from the endpoint.
 *
 * @param array $projects
 *   Default value.
 */
function openm_update_get_projects(&$projects) {
  $client = \Drupal::httpClient();

  try {
    $response = $client->post(openm_UPDATE_PACKAGES_ENDPOINT, [
      'body' => json_encode($projects),
      'connect_timeout' => 10,
      'read_timeout' => 20,
      'timeout' => 30
    ]);
    if ($response->getStatusCode() == 200) {
      $body = $response->getBody();
      $body = $body->getContents();
      $projects = json_decode($body, TRUE);
    }
  }
  catch (\Exception $e) {
    \Drupal::logger('openm_update')->warning('Failed to connect to %endpoint', ['%endpoint' => openm_UPDATE_PACKAGES_ENDPOINT]);
  }
}

/**
 * Implements hook_update_status_alter().
 */
function openm_update_update_status_alter(&$projects) {
  openm_update_get_projects($projects);
}

/**
 * Implements hook_update_projects_alter().
 */
function openm_update_update_projects_alter(&$projects) {
  openm_update_get_projects($projects);
}
