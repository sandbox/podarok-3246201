<?php

namespace Drupal\openm_repeat_entity\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a Repeat entity.
 * @ingroup openm_repeat_entity
 */
interface RepeatInterface extends ContentEntityInterface {

}
