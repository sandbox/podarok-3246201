<?php
/**
 * Open M Robo commands. 
 * Here we are able to create an any version of Open M for CI builds.
 *
 * @see http://robo.li/
 */
class RoboFile extends \Robo\Tasks {
  /**
   * Create Open M project https://github.com/openm/openm-project without installation.
   *
   * @param string $path
   *   Installation path that will be used to create "openm-project" folder.
   */
  function OpenmCreateProject($path) {
    $this->taskComposerCreateProject()
      ->source('openm/openm-project:9.2.x-init-dev')
      ->target($path . '/openm-project')
      ->ansi(TRUE)
      ->noInstall(TRUE)
      ->noInteraction()
      ->run();
  }

  /**
   * Add fork as repository to composer.json.
   *
   * @param string $path
   *   Installation path where repository should be added.
   *
   * @param string $repository
   *   Local path of the repository.
   */
  function OpenmAddFork($path, $repository) {
    $this->taskComposerConfig()
      ->dir($path . '/openm-project')
      ->repository(99, $repository, 'path')
      ->ansi(TRUE)
      ->run();
  }


  /**
   * Set target branch of the fork.
   *
   * @param string $path
   *   Installation path where "openm-project" is placed.
   *
   * @param string $branch
   *   Branch name.
   */
  function OpenmSetBranch($path, $branch) {
    $this->taskComposerRequire()
      ->dir($path . '/openm-project')
      ->dependency('openm/openm', $branch)
      ->ansi(TRUE)
      ->run();
  }

  /**
   * Installs Open M from fork as dependency.
   *
   * @param string $path
   *   Installation path where "openm-project" is placed.
   */
  function OpenmInstall($path) {
    $this->taskComposerInstall()
      ->dir($path . '/openm-project')
      ->noInteraction()
      ->ansi(TRUE)
      ->run();
  }

  /**
   * Creates symlink to mirror build folder into web accessible dir.
   *
   * @param string $docroot_path
   *   Path where website folder should be created.
   *
   * @param string $build_path
   *   Path where source code for build is placed.
   */
  function OpenmBuildFolder($docroot_path, $build_path) {
    $this->taskFilesystemStack()
      ->symlink($docroot_path, $build_path)
      ->chgrp('www-data', 'jenkins')
      ->run();
  }
}
