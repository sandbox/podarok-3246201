# Open M Distribution
The Open M platform is a content management system that uses Drupal 8 functionality and useful modules from OPENMs and digital partners. It’s easy and free to use—everyone is welcome to implement Open M and run Open M projects.

In 2016 a group of OPENM digital, marketing and technology experts recognized the digital opportunities that exist if we work together as a community and established Open M.

A core team led by a small group of OPENMs including the [Greater ](http://www.openmmn.org), [Greater Seattle](http://www.seattleopenm.org) and [Greater Houston](https://www.openmhouston.org):

- Pays for expenses associated with managing Open M
- Maintains the Open M content management system
- Ensures all basic functionality accessible from the content management system is available free of charge—those who contribute cannot charge others for what is shared
- Strives to be aware of issues found within the Open M content management system
- Is not liable for bugs, crashes or performance issues of the content management system
- Invites and approves digital partners to join
- Offers training for Open M Specialists—digital partners that are very familiar with the platform
- Offers certification for Open M Integrators—digital partners that can install and work directly on the codebase
- Distributes communication about Open M
- Organizes events for the Open M community—including an annual meeting each June
 
Open M is similar to the [Thunder Coalition](http://www.thunder.org) for the publishing industry, which has generously agreed to share some of the same concepts and content that you see used on this site.

# Documentation
Documentation is available at [https://github.com/openm/openm/tree/8.x-1.x/docs](https://github.com/openm/openm/tree/8.x-1.x/docs)

# Details
For details please visit http://www.openmmca.org
