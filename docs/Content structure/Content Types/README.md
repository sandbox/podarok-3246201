Welcome to the Open M Content Types documentation

In terms of Open M - content types are ```bundles``` of ```node``` entity of the Drupal Framework.
You can find a much more low level documentation at [drupal.org](https://www.drupal.org/docs/8).

Open M has a bunch of content types shipped for the convenience of usage the resulting site.
We are not limiting amount of content types, could be added by developers, so the list is not final.
The only rule we are trying to follow is to cover shipped list of content types by Open M upgrade path.
