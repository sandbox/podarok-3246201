Here you can find instructions how you can patch Open M distribution used on 
your project.

# When you need to patch Open M
- In case you found a bug and prepared a patch for Open M on github.
- In case you developed a new feature that will be good to have in Open M and 
created Pull Request to Open M repository
- In case you want to add a feature that added to Open M but not included yet to 
Open M release.

# How to patch Open M via composer?

If you followed instructions [docs/Development/Start new Open M project](https://github.com/openm/openm/blob/8.x-1.x/docs/Development/Start%20new%20OpenM%20project.md)
and you have configured `composer.json` you need just to do a few simple steps:
1. Build a link to a patch using pull request ID
    ```
    https://patch-diff.githubusercontent.com/raw/openm/openm/pull/XXX.patch
    ```
Where XXX is a number of pull request you want to use. 

2. Add a new section `patches` to the section `extra` and add a patch to Open M 
repository, as on this example:
    ```
    "extra": {
        "installer-paths": {
          ...
        },
        "enable-patching": true,
        "patches": {
            "openm/openm": {
                "Patch description": "https://patch-diff.githubusercontent.com/raw/openm/openm/pull/XXX.patch"
            }
        }
    }
    ```
3. After adding a patch execute command `composer update`
4. Verify you can see added changes in Open M
5. Enjoy!
