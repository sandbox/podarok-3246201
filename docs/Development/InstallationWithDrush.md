### Site installation with drush

Use [drush site-install](https://drushcommands.com/drush-8x/core/site-install/) command.

Basically you use something like this:
```
drush site-install openm --account-pass=password --db-url="mysql://user:pass@host:3306/db" --root=/var/www/docroot
```

Complete Open M profile preset and Open M Rose theme is used in this case.

You can set which preset must be installed by specifying it with `openm_configure_profile.preset` variable, and theme with 
`openm_theme_select.theme`variable e.g.:
```
drush site-install openm --account-pass=password --db-url="mysql://user:pass@host:3306/db" --root=/var/www/docroot openm_configure_profile.preset=extended openm_theme_select.theme=openm_rose
```
